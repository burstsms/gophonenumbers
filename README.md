Small wrapper for googles libphonenumber C++ libs using cgo for golang

[![Build Status](https://drone.io/bitbucket.org/burstsms/gophonenumbers/status.png)](https://drone.io/bitbucket.org/burstsms/gophonenumbers/latest)

Changelog:

v0.0.1 - Added functions for testing number validity and formatting as international

Note:

Requires googles libphonenumber, boackports for ubuntu 12.04 and 14.04 can be found at this ppa:

https://launchpad.net/~b-mike-n/+archive/ubuntu/burstsms-ppa