#include "phone.h"
#include "stdio.h"
#include "phonenumbers/phonenumberutil.h"

using namespace i18n;
using namespace phonenumbers;

ParseResult* parseNumber(const char *input, const char *country_code) {

    ParseResult *r = (ParseResult *)malloc(sizeof(ParseResult));

    const PhoneNumberUtil* const phone_util = PhoneNumberUtil::GetInstance(); 
    PhoneNumber test_number;

    phone_util->Parse(string(input), country_code, &test_number);

    bool isvalid = false;

    isvalid = phone_util->IsValidNumber(test_number);

    r->isValid = isvalid;
    r->CountryCode = test_number.country_code();
    r->NationalNumber = test_number.national_number();

    return r;
}
