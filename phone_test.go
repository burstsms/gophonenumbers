package gophonenumbers_test

import (
	. "bitbucket.org/burstsms/gophonenumbers"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Phone", func() {

	Describe("Checking phone number validity", func() {
		Context("With a valid number and country code", func() {
			It("Should find phone number valid", func() {
				Expect(ValidPhoneNumber("0422265404", "AU")).To(BeTrue(), "valid phone number is valid")
			})
		})
		Context("With a valid number and an invalid country code", func() {
			It("Should be false", func() {
				Expect(ValidPhoneNumber("0422265404", "ZZZ")).To(BeFalse(), "valid phone number is false with invalid countrycode")
			})
		})
		Context("With am invalid number and an invalid country code", func() {
			It("Should be false", func() {
				Expect(ValidPhoneNumber("4225404", "ZZZ")).To(BeFalse(), "invalid phone number is false with invalid countrycode")
			})
		})

	})

	Describe("Formatting to international format", func() {
		Context("With a valid local number and country code", func() {
			It("Should find phone number valid and return in international format", func() {
				Expect(PhoneNumberAsInternational("0422265404", "AU")).To(Equal("61422265404"), "valid phone number is valid")
			})
		})
		Context("With a valid international number and country code", func() {
			It("Should find phone number valid and return in international format", func() {
				Expect(PhoneNumberAsInternational("61422265404", "AU")).To(Equal("61422265404"), "valid phone number is valid")
			})
		})
		Context("With a valid number and an invalid country code", func() {
			It("Should be empty string", func() {
				Expect(PhoneNumberAsInternational("0422265404", "ZZZ")).To(Equal(""), "valid phone number is false with invalid countrycode")
			})
		})
		Context("With am invalid number and an invalid country code", func() {
			It("Should be empty string", func() {
				Expect(PhoneNumberAsInternational("4225404", "ZZZ")).To(Equal(""), "invalid phone number is false with invalid countrycode")
			})
		})

	})

})
