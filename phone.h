#ifndef MAIN_H_
#define MAIN_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    unsigned int isValid;
    unsigned int CountryCode;
    long unsigned int NationalNumber;
} ParseResult;

ParseResult* parseNumber(const char*,const char*);

#ifdef __cplusplus
}
#endif
#endif  // MAIN_H_
