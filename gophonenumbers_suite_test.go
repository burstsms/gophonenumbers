package gophonenumbers_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
)

func TestGophonenumbers(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Gophonenumbers Suite")
}
