package gophonenumbers

import (
	"strconv"
	"unsafe"
)

/*
#cgo LDFLAGS: -lphonenumber

#include "phone.h"
#include "stdlib.h"

*/
import "C"

func ValidPhoneNumber(gonumber string, gocountrycode string) bool {
	isValid := false

	number := C.CString(gonumber)
	defer C.free(unsafe.Pointer(number))
	countrycode := C.CString(gocountrycode)
	defer C.free(unsafe.Pointer(countrycode))

	numberresult := C.parseNumber(number, countrycode)
	defer C.free(unsafe.Pointer(numberresult))

	isValid = numberresult.isValid != 0

	return isValid
}

func PhoneNumberAsInternational(gonumber string, gocountrycode string) string {
	output := ""

	number := C.CString(gonumber)
	defer C.free(unsafe.Pointer(number))
	countrycode := C.CString(gocountrycode)
	defer C.free(unsafe.Pointer(countrycode))

	numberresult := C.parseNumber(number, countrycode)
	defer C.free(unsafe.Pointer(numberresult))

	if numberresult.isValid != 0 {
		output = strconv.Itoa(int(numberresult.CountryCode)) + strconv.Itoa(int(numberresult.NationalNumber))
	}

	return output
}
